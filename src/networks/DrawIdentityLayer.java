package networks;

import javafx.scene.canvas.Canvas;

import java.util.List;

/**
 * draws the nodes of the layer with out any edges
 */
public class DrawIdentityLayer implements LayerDrawer {

    public void drawLayer(Canvas canvas, List<Node> previousNodes, List<Node> currentNodes) {
        for (Node node : currentNodes) { node.drawNode(canvas); }
    }
}
