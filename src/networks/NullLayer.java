package networks;

import javafx.scene.canvas.Canvas;

public class NullLayer extends AbstractLayer {
    public NullLayer() {
        super(new Canvas(), 0, 0);
    }
}
