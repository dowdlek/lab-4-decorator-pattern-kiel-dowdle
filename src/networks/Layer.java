package networks;

import java.util.List;

public interface Layer {

    /**
     * returns the number of nodes in this layer
     * @return number of nodes in this layer
     */
    int getSize();

    /**
     * returns which number layer this is in the network
     * @return the number of the layer
     */
    int getCount();

    /**
     * returns a list of nodes in this layer
     * @return the list of nodes in this layer
     */
    List<Node> getNodes();

    /**
     * draws the layer to the canvas
     */
    void drawLayer();

    /**
     * sets the reference to the previous layer in the network
     * @param previousLayer the previous layer of the network
     */
    void setPreviousLayer(Layer previousLayer);

    /**
     * gets a reference to the previous layer in the network
     * @return the previous layer in the network
     */
    Layer getPreviousLayer();

    /**
     * gets a reference to the next layer in the network
     * @return the next layer in the network
     */
    Layer getNextLayer();

    /**
     * sets the strategy for how this layer is drawn
     * @param layerDrawer the strategy for this type of layer
     */
    void setLayerDrawer(LayerDrawer layerDrawer);

    /**
     * set the reference to the next layer in the network
     * @param layer reference to the next layer in the network
     */
    void setNextLayer(Layer layer);

}
