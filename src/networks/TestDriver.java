package networks;

import javafx.scene.canvas.Canvas;

public class TestDriver {

    public static void main(String[] args) {

        Canvas canvas = new Canvas();

        Layer network = new IdentityLayer(canvas, 4);
        network.drawLayer();

        network = new OneByOneConnectedLayer(canvas, network);
        network.drawLayer();

        network = new OneByOneConnectedLayer(canvas, network);
        network.drawLayer();

        network = new FullyConnectedLayer(canvas, network, 4);
        network.drawLayer();

        network = new FullyConnectedLayer(canvas, network, 3);
        network.drawLayer();
    }

}
