package networks;

import javafx.scene.canvas.Canvas;

public class FullyConnectedLayer extends AbstractDecoratedLayer {

    public FullyConnectedLayer(Canvas canvas, Layer layer, int size) {
        super(canvas, layer, size);
        this.setLayerDrawer(new DrawFullLayer());
    }

}
