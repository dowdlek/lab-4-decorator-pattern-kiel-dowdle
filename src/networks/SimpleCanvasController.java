/*
 * Course:     SE 2811
 * Term:       Winter 2019-20
 * Assignment: Lab 4: Decorators
 * Author: Dr. Yoder and Kiel Dowdle
 * Date: 1/28/2020
 */
package networks;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;

import java.util.HashMap;
import java.util.Map;

/**
 * The controller for the main window.
 *
 * Also manages the networks.
 */
public class SimpleCanvasController {

    @FXML
    private Canvas canvas;

    @FXML
    private Label networkSize, inputLayerSize, outputLayerSize;

    private Map<String, Network> networks = new HashMap<>();

    @FXML
    private void showNetwork(ActionEvent actionEvent) {
        ToggleButton source = (ToggleButton)actionEvent.getSource();
        String id = source.getId();

        GraphicsContext context = canvas.getGraphicsContext2D();

        context.setLineWidth(1);
        context.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

        if(!networks.containsKey(id)) {
            System.out.println("Warning: Unknown network id:"+id);
        } else {
            Network network = networks.get(id);
            network.draw();
            inputLayerSize.setText(Integer.toString(network.inputSize()));
            outputLayerSize.setText(Integer.toString(network.outputSize()));
            networkSize.setText(Integer.toString(network.numLayers()));
        }
    }

   @FXML
    private void initialize() {
        networks.put("alexLike",createAlexNet());
        networks.put("inceptionLike",createInception());
        networks.put("testNet",createTestNet());
    }

    /**
     * As client code, use the decorator classes to construct the inception-like network,
     * as described in the lab.
     * @return network The network created.
     */
    @FXML
    private Network createInception() {
        Network network = new Network(canvas, 3);
        network.addOneToOneLayer();
        network.addOneToOneLayer();
        network.addOneToOneLayer();
        return network;
    }

    /**
     * As client code, use the decorator classes to construct the AlexNet-like network,
     * as described in the lab.
     * @return network The network created.
     */
    private Network createAlexNet() {
        Network network = new Network(canvas, 6);
        network.addOneToOneLayer();
        network.addOneToOneLayer();
        network.addFullyConnectedLayer(4);
        network.addFullyConnectedLayer(3);
        return network;
    }

    /**
     * test network
     * @return network The network created.
     */
    private Network createTestNet() {
        Network network = new Network(canvas, 14);
        network.addOneToOneLayer();
        network.addOneToOneLayer();
        network.addFullyConnectedLayer(4);
        network.addFullyConnectedLayer(3);
        return network;
    }



}
