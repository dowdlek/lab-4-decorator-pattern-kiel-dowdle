package networks;

import javafx.scene.canvas.Canvas;

public class OneByOneConnectedLayer extends AbstractDecoratedLayer {

    public OneByOneConnectedLayer(Canvas canvas, Layer layer) {
        super(canvas, layer, layer.getSize());
        this.setLayerDrawer(new DrawOneByOneLayer());
    }

}
