package networks;

import javafx.scene.canvas.Canvas;

public class IdentityLayer extends AbstractLayer {

    public IdentityLayer(Canvas canvas, int size) {
        super(canvas, 1, size);
        this.setPreviousLayer(new NullLayer());
        this.setLayerDrawer(new DrawIdentityLayer());
    }

}
