package networks;

import javafx.scene.canvas.Canvas;

import java.util.List;

/**
 * draws the nodes of this layer and the edges connecting each node to all the nodes in the previous layer
 */
public class DrawFullLayer implements LayerDrawer {

    public void drawLayer(Canvas canvas, List<Node> previousNodes, List<Node> currentNodes) {

        for (Node currentNode : currentNodes) {
            currentNode.drawNode(canvas);
            for (Node previousNode : previousNodes) {
                currentNode.drawEdge(canvas, previousNode);
            }
        }
    }
}
