/*
 * Course: SE-2811-051
 * Name: Kiel Dowdle
 * Created: 1/28/2020
 */

package networks;

import javafx.scene.canvas.Canvas;

/**
 * This class is wrapped around the Layer interface and provides functions for building a
 * neural network from the layers. This class implements the decorator pattern for building up
 * the layers of the network.
 */
public class Network {

    private Layer identityLayer;
    private Canvas canvas;
    private Layer neuralNet;
    private int size;

    /**
     * Constructs a network. A network has at least one layer. The identity layer is constructed with the given size.
     * @param canvas the canvas which the network will be drawn to.
     * @param identityLayerSize the size of the identity layer.
     */
    public Network(Canvas canvas, int identityLayerSize) {
        this.identityLayer = new IdentityLayer(canvas, identityLayerSize);
        this.neuralNet = identityLayer;
        this.canvas = canvas;
        size = 1;
    }

    /**
     * adds a fully connected layer to the network using the decorator pattern.
     * @param nodes the number of nodes in the fully connected layer.
     */
    public void addFullyConnectedLayer(int nodes) {
        neuralNet = new FullyConnectedLayer(canvas, neuralNet, nodes);
        size++;
    }

    /**
     * adds a 1 to 1 layer to the network using the decorator patter.
     */
    public void addOneToOneLayer() {
        neuralNet = new OneByOneConnectedLayer(canvas, neuralNet);
        size++;
    }

    /**
     * draws the network to the canvas.
     */
    public void draw(){
        Layer layer = this.identityLayer;
        while (layer != null) {
            layer.drawLayer();
            layer = layer.getNextLayer();
        }
    }

    /**
     * returns the number of layers in the network.
     * @return the number of layers in the network.
     */
    public int numLayers() { return this.size; }

    /**
     * returns the size of the input layer for the network.
     * @return number of nodes in the input layer.
     */
    public int inputSize() { return this.identityLayer.getSize(); }

    /**
     * returns the size of the output layer for the network.
     * @return number of nodes in the output layer.
     */
    public int outputSize() { return this.neuralNet.getSize(); }
}
