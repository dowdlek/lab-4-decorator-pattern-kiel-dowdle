package networks;

import javafx.scene.canvas.Canvas;
import java.util.List;

/**
 * draws the nodes of the layer and the edges from these nodes to the nodes directly behind them
 */
public class DrawOneByOneLayer implements LayerDrawer {

    public void drawLayer(Canvas canvas, List<Node> previousNodes, List<Node> currentNodes) {
        for (int i = 0; i < currentNodes.size(); i++) {
            currentNodes.get(i).drawNode(canvas);
            currentNodes.get(i).drawEdge(canvas, previousNodes.get(i));
        }
    }
}
