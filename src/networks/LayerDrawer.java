package networks;

import javafx.scene.canvas.Canvas;
import java.util.List;


/**
 * Implementation of the strategy pattern for the behavior of drawing the layer on the canvas.
 */
public interface LayerDrawer {

    /**
     * A method each layer must have. Used to draw the layer to the canvas.
     * @param canvas the canvas to draw the layer on
     * @param previousNodes The nodes where the edges are drawn to
     * @param currentNodes the nodes to be drawn and where the edges start
     */
    void drawLayer(Canvas canvas, List<Node> previousNodes, List<Node> currentNodes);
}
