package networks;

import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

/**
 * A class to represent a node on the canvas. Has the responsibility to hold the location of the node and
 * its radius. It also has the responsibility to draw the node its corresponding edge on the canvas.
 */
public class Node {

    private final int RADIUS = 10;
    private double xCenter;
    private double yCenter;

    /**
     * creates a node given the center point of the node
     * @param x the x coordinate of the center of the node
     * @param y the y coordinate of the center of the node
     */
    public Node(double x, double y){
        this.xCenter = x;
        this.yCenter = y;
    }

    /**
     * draws the node onto the canvas
     * @param canvas the canvas where the node is drawn
     */
    public void drawNode(Canvas canvas) {
        canvas.getGraphicsContext2D().strokeOval(xCenter-RADIUS,yCenter-RADIUS,2*RADIUS,2*RADIUS);
    }

    /**
     * draws the edge between two nodes
     * @param canvas the canvas where the node is drawn
     * @param otherNode the other node that the drawn edge connects
     */
    public void drawEdge(Canvas canvas, Node otherNode) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        javafx.geometry.Point2D p1 = new javafx.geometry.Point2D(this.xCenter, this.yCenter);
        javafx.geometry.Point2D p2 = new javafx.geometry.Point2D(otherNode.xCenter, otherNode.yCenter);
        javafx.geometry.Point2D direction = p2.subtract(p1).normalize();
        javafx.geometry.Point2D radius = direction.multiply(RADIUS);
        javafx.geometry.Point2D start = p1.add(radius);
        Point2D end = p2.subtract(radius);

        context.strokeLine(start.getX(), start.getY(), end.getX(), end.getY());
    }

    @Override
    public String toString() {
        return "Node x: " + xCenter + " y: " + yCenter;
    }

}
