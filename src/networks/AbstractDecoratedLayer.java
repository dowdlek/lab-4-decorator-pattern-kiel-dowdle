package networks;

import javafx.scene.canvas.Canvas;

public abstract class AbstractDecoratedLayer extends AbstractLayer {

    public AbstractDecoratedLayer(Canvas canvas, Layer layer, int size) {
        super(canvas, layer.getCount() + 1, size);
        this.setPreviousLayer(layer);
        layer.setNextLayer(this);
    }

}
