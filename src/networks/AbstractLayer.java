package networks;

import javafx.scene.canvas.Canvas;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractLayer implements Layer {

    private Canvas canvas;
    private LayerDrawer layerDrawer;
    private Layer previousLayer;
    private Layer nextLayer;
    private List<Node> nodes;
    private int size;
    private int count;


    private double xDistance;
    private double xOffset;
    private double yDistance;

    /**
     * Creates the layer with the desired number of nodes.
     *
     * @param canvas The canvas where the nodes will be drawn.
     * @param count the current number of layers the network has
     * @param size the number of nodes to make for this layer
     */
    public AbstractLayer(Canvas canvas, int count, int size) {
        this.count = count;
        this.size = size;
        this.nextLayer = null;
        this.canvas = canvas;

        this.xDistance = (canvas.getWidth() / 10 );
        this.xOffset = xDistance / 2;
        this.yDistance = canvas.getHeight() / (size + 1);

        nodes = generateNodes();
    }

    /*
    Implementations of the methods described in the Layer interface
     */

    public void drawLayer() { this.layerDrawer.drawLayer(canvas, previousLayer.getNodes(), nodes); }

    public void setLayerDrawer(LayerDrawer layerDrawer) { this.layerDrawer = layerDrawer; }

    public void setPreviousLayer(Layer previousLayer) { this.previousLayer = previousLayer; }

    public void setNextLayer(Layer nextLayer) {this.nextLayer = nextLayer; }

    public Layer getPreviousLayer() { return previousLayer; }

    public Layer getNextLayer() { return nextLayer; }

    public int getCount() { return count; }

    public List<Node> getNodes() { return nodes; }

    public int getSize() { return this.size; }

    @Override
    public String toString() { return this.getClass().getSimpleName() + " size: " + size + " number: " + count; }

    /**
     * Creates all the nodes for this layer and gives them coordinates for their correct position on the canvas.
     *
     * @return the list of nodes for this layer
     */
    private List<Node> generateNodes(){
        List<Node> currentNodes = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            currentNodes.add(new Node((xDistance * (count - 1) ) + xOffset, ( (i + 1) * yDistance)));
        }
        return currentNodes;
    }



}
